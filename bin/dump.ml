open Containers

let () =
  match Sys.argv with
  | [| _; midifile |] ->
      Midifile.read_from_file midifile |> Midifile.pp Format.stdout;
      Format.print_newline ();
      Format.flush Format.stdout ()
  | [| _; "bytes"; midifile |] ->
      Midifile.read_from_file midifile
      |> Midifile.write_to_channel stdout
      |> ignore
  | [| _; "simple"; midifile |] ->
      Midifile.read_from_file midifile
      |> Midifile.Simple_event_list.Input.of_midifile
      |> Midifile.Simple_event_list.Input.pp Format.stdout;
      Format.print_newline ();
      Format.flush Format.stdout ()
  | _ ->
      Printf.eprintf
        "Bad args. Usage: dump.exe MIDIFILE | dump.exe bytes MIDIFILE\n\
        \ | dump.exe simple MIDIFILE\n";
      exit 1
