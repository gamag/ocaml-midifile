clean: phony
	dune clean

# Tests

test: phony
	dune runtest

dump: phony
	dune exec bin/dump.exe -- "test/midi/$(case).mid"

diff_expected: phony
	dune exec bin/dump.exe -- "test/midi/$(case).mid" | diff "test/expected/$(case).mid.expected" -

update_expected: phony
	dune exec bin/dump.exe -- "test/midi/$(case).mid" > "test/expected/$(case).mid.expected"

dump_simple: phony
	dune exec bin/dump.exe -- simple "test/midi/$(case).mid"

diff_expected_simple: phony
	dune exec bin/dump.exe -- simple "test/midi/$(case).mid" | diff "test/expected_simple/$(case).mid.expected" -

update_expected_simple: phony
	dune exec bin/dump.exe -- simple "test/midi/$(case).mid" > "test/expected_simple/$(case).mid.expected"

phony:

.PHONY: phony
