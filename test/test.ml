open Containers
open Alcotest

let division =
  ( module struct
    type t = Midifile.division

    let equal = Midifile.equal_division

    let pp = Midifile.pp_division
  end : TESTABLE
    with type t = Midifile.division )

type midi_case = {
  basename : string;
  midi_file : string;
  expected_file : string;
  expected_simple_file : string;
}

let midi_files =
  Bos.OS.Dir.contents (Fpath.of_string "../../../test/midi" |> Rresult.R.get_ok)
  |> Rresult.R.get_ok
  |> List.map (fun x ->
         let basename = Fpath.basename x in
         {
           basename;
           midi_file = Fpath.to_string x;
           expected_file = "../../../test/expected/" ^ basename ^ ".expected";
           expected_simple_file =
             "../../../test/expected_simple/" ^ basename ^ ".expected";
         })

let check_diff_file_and_string case_name file str =
  check string case_name ""
    Bos.(
      OS.Cmd.in_string str
      |> OS.Cmd.run_io Cmd.(v "diff" % file % "-")
      |> OS.Cmd.out_string |> Rresult.R.get_ok |> fst)

let midifile_cases =
  List.map
    (fun { basename; midi_file; expected_file; expected_simple_file } ->
      test_case basename `Quick (fun () ->
          let read_bytes = CCIO.with_in midi_file CCIO.read_all_bytes in
          let read_obj = Midifile.read_from_bytes read_bytes in
          let read_size_in_bytes = Midifile.size_in_bytes read_obj in
          (* <= because the input file may not use running bytes fully and the
           * the library always does *)
          check bool "read_size_in_bytes_leq_original_bytes" true
            (read_size_in_bytes <= Bytes.length read_bytes);
          let read_dump = Midifile.show read_obj ^ "\n" in
          check_diff_file_and_string "read_dump_diff" expected_file read_dump;
          let written_bytes = Bytes.create read_size_in_bytes in
          let written_bytes_bytes_written =
            Midifile.write_to_bytes written_bytes read_obj
          in
          check int "bytes_written_eq_read_size_in_bytes" read_size_in_bytes
            written_bytes_bytes_written;
          let written_obj = Midifile.read_from_bytes written_bytes in
          check
            (module Midifile.Representation)
            "read_obj_eq_written_obj" read_obj written_obj;
          match read_obj.division with
          | Ticks _ ->
              let simple_dump =
                Midifile.Simple_event_list.Input.(of_midifile read_obj |> show)
                ^ "\n"
              in
              check_diff_file_and_string "read_simple_dump_diff"
                expected_simple_file simple_dump
          | SMPTE _ ->
              check_raises "SMPTE_raises"
                (Invalid_argument "SMPTE not supported") (fun () ->
                  Midifile.Simple_event_list.Input.of_midifile read_obj
                  |> ignore)))
    midi_files

let () = run "test" [ ("midifile", midifile_cases) ]
