open Containers
open Representation

module type Sink = sig
  type t

  val set_uint8 : pos:int -> t -> int -> unit

  val set_uint16_be : pos:int -> t -> int -> unit

  val set_uint32_be : pos:int -> t -> int -> unit

  val blit_string : pos:int -> t -> string -> unit
end

module type S = sig
  type sink

  val write : ?offset:int -> sink -> Representation.t -> int
end

let pow2log2 x =
  let rec impl count x =
    match x with 1 -> count | _ -> impl (count + 1) (x lsr 1)
  in
  impl 0 x

let int_of_division division =
  match division with
  | Ticks x -> x
  | SMPTE (fps, x) -> (
      x
      lor
      match fps with
      | Fps_24 -> 0xe600
      | Fps_25 -> 0xe700
      | Fps_30_drop_frame -> 0xe800
      | Fps_30_non_drop_frame -> 0xe900 )

let int_of_format = function
  | Single_track -> 0
  | Simultaneous_tracks -> 1
  | Sequential_tracks -> 2

module Make (W : Sink) : S with type sink = W.t = struct
  type sink = W.t

  let set_int24_be ~pos b x =
    W.set_uint8 b ~pos ((x land 0xff0000) lsr 16);
    W.set_uint8 b ~pos:(pos + 1) ((x land 0x00ff00) lsr 8);
    W.set_uint8 b ~pos:(pos + 2) (x land 0x0000ff)

  let write_var_int32 ~pos b x =
    let buffer = ref (x land 0x7f) in
    let x = ref (x lsr 7) in
    while !x > 0 do
      let byte = !x land 0x7f lor 0x80 in
      buffer := (!buffer lsl 8) lor byte;
      x := !x lsr 7
    done;
    let pos = ref pos in
    while !buffer land 0x80 > 0 do
      W.set_uint8 ~pos:!pos b (!buffer land 0xff);
      buffer := !buffer lsr 8;
      incr pos
    done;
    W.set_uint8 ~pos:!pos b (!buffer land 0xff);
    !pos + 1

  let write_string ~pos b txt =
    let length = String.length txt in
    let pos = write_var_int32 ~pos b length in
    W.blit_string ~pos b txt;
    pos + length

  let write_meta_event ~pos b data =
    let write_tagged_string ~pos b tag txt =
      W.set_uint8 ~pos b tag;
      write_string ~pos:(pos + 1) b txt
    in
    W.set_uint8 ~pos b 0xff;
    let pos = pos + 1 in
    match data with
    | Sequence_number None ->
        W.set_uint8 ~pos b 0x00;
        W.set_uint8 ~pos:(pos + 1) b 0x00;
        pos + 2
    | Sequence_number (Some x) ->
        W.set_uint8 ~pos b 0x00;
        W.set_uint8 ~pos:(pos + 1) b 0x02;
        W.set_uint16_be ~pos:(pos + 2) b x;
        pos + 4
    | Text txt -> write_tagged_string ~pos b 0x01 txt
    | Copyright txt -> write_tagged_string ~pos b 0x02 txt
    | Sequence_track_name txt -> write_tagged_string ~pos b 0x03 txt
    | Instrument_name txt -> write_tagged_string ~pos b 0x04 txt
    | Lyric txt -> write_tagged_string ~pos b 0x05 txt
    | Marker txt -> write_tagged_string ~pos b 0x06 txt
    | Cue_point txt -> write_tagged_string ~pos b 0x07 txt
    | Program_name txt -> write_tagged_string ~pos b 0x08 txt
    | Device_name txt -> write_tagged_string ~pos b 0x09 txt
    | Midi_channel_prefix ch ->
        W.set_uint8 ~pos b 0x20;
        W.set_uint8 ~pos:(pos + 1) b 0x01;
        W.set_uint8 ~pos:(pos + 2) b ch;
        pos + 3
    | Midi_port ch ->
        W.set_uint8 ~pos b 0x21;
        W.set_uint8 ~pos:(pos + 1) b 0x01;
        W.set_uint8 ~pos:(pos + 2) b ch;
        pos + 3
    | End_of_track ->
        W.set_uint8 ~pos b 0x2f;
        W.set_uint8 ~pos:(pos + 1) b 0x00;
        pos + 2
    | Tempo x ->
        W.set_uint8 ~pos b 0x51;
        W.set_uint8 ~pos:(pos + 1) b 0x03;
        set_int24_be ~pos:(pos + 2) b x;
        pos + 5
    | SMPTE_offset { fps; hour; minute; second; frame; subframe } ->
        let fps_hour =
          hour
          lor
          match fps with
          | Fps_24 -> 0b0_00_00000
          | Fps_25 -> 0b0_01_00000
          | Fps_30_drop_frame -> 0b0_10_00000
          | Fps_30_non_drop_frame -> 0b0_11_00000
        in
        W.set_uint8 ~pos b 0x54;
        W.set_uint8 ~pos:(pos + 1) b 0x05;
        W.set_uint8 ~pos:(pos + 2) b fps_hour;
        W.set_uint8 ~pos:(pos + 3) b minute;
        W.set_uint8 ~pos:(pos + 4) b second;
        W.set_uint8 ~pos:(pos + 5) b frame;
        W.set_uint8 ~pos:(pos + 6) b subframe;
        pos + 7
    | Time_sig { num; den; clocks; div } ->
        W.set_uint8 ~pos b 0x58;
        W.set_uint8 ~pos:(pos + 1) b 0x04;
        W.set_uint8 ~pos:(pos + 2) b num;
        W.set_uint8 ~pos:(pos + 3) b (pow2log2 den);
        W.set_uint8 ~pos:(pos + 4) b clocks;
        W.set_uint8 ~pos:(pos + 5) b div;
        pos + 6
    | Key_sig { alter; mode } ->
        W.set_uint8 ~pos b 0x59;
        W.set_uint8 ~pos:(pos + 1) b 0x02;
        W.set_uint8 ~pos:(pos + 2) b alter;
        W.set_uint8 ~pos:(pos + 3) b
          (match mode with Major_key -> 0 | Minor_key -> 1);
        pos + 4
    | Sequencer_specific_data txt -> write_tagged_string ~pos b 0x7f txt

  let write_midi_event ~pos b running_status channel data =
    let write_status ~pos b running_status status =
      if running_status = status then pos
      else (
        W.set_uint8 ~pos b status;
        pos + 1 )
    in
    match data with
    | Note_off { pitch; velocity } ->
        let status = 0x80 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b pitch;
        W.set_uint8 ~pos:(pos + 1) b velocity;
        (pos + 2, status)
    | Note_on { pitch; velocity } ->
        let status = 0x90 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b pitch;
        W.set_uint8 ~pos:(pos + 1) b velocity;
        (pos + 2, status)
    | Polyphonic_pressure { pitch; pressure } ->
        let status = 0xa0 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b pitch;
        W.set_uint8 ~pos:(pos + 1) b pressure;
        (pos + 2, status)
    | Control_change { controller; value } ->
        let status = 0xb0 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b controller;
        W.set_uint8 ~pos:(pos + 1) b value;
        (pos + 2, status)
    | Program_change program ->
        let status = 0xc0 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b program;
        (pos + 1, status)
    | Channel_pressure pressure ->
        let status = 0xd0 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b pressure;
        (pos + 1, status)
    | Pitch_wheel_change pressure ->
        let status = 0xe0 lor channel in
        let pos = write_status ~pos b running_status status in
        W.set_uint8 ~pos b pressure;
        (pos + 1, status)

  let write_sysex_event ~pos b data =
    match data with
    | Multi_packet_begin txt | Single_packet txt ->
        W.set_uint8 ~pos b 0xf0;
        write_string ~pos:(pos + 1) b txt
    | Multi_packet_middle txt | Multi_packet_end txt | Escaped txt ->
        W.set_uint8 ~pos b 0xf7;
        write_string ~pos:(pos + 1) b txt

  let write_event ~pos b running_status event =
    match event with
    | Meta_event (delta_time, data) ->
        let pos = write_var_int32 ~pos b delta_time in
        (write_meta_event ~pos b data, 0)
    | Sysex_event (delta_time, data) ->
        let pos = write_var_int32 ~pos b delta_time in
        (write_sysex_event ~pos b data, 0)
    | Midi_event (delta_time, { channel; data }) ->
        let pos = write_var_int32 ~pos b delta_time in
        write_midi_event ~pos b running_status channel data

  let write_track ~pos b { size_in_bytes; events } =
    W.blit_string ~pos b "MTrk";
    W.set_uint32_be ~pos:(pos + 4) b size_in_bytes;
    List.fold_left
      (fun (pos, running_status) ev -> write_event ~pos b running_status ev)
      (pos + 8, 0)
      events
    |> fst

  let write_header ~pos b format ntracks division =
    W.blit_string ~pos b "MThd";
    W.set_uint32_be ~pos:(pos + 4) b 6;
    W.set_uint16_be ~pos:(pos + 8) b (int_of_format format);
    W.set_uint16_be ~pos:(pos + 10) b ntracks;
    W.set_uint16_be ~pos:(pos + 12) b (int_of_division division);
    pos + 14

  let write ?(offset = 0) b { format; division; tracks } =
    let offset =
      write_header ~pos:offset b format (List.length tracks) division
    in
    List.fold_left
      (fun offset track -> write_track ~pos:offset b track)
      offset tracks
end
