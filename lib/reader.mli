exception Invalid_header_prefix of string * int

exception Invalid_header of int * int * int

exception Invalid_status of int

exception Invalid_meta_event of int * int

module type Source = sig
  type t

  val get_uint8 : pos:int -> t -> int

  val get_uint16_be : pos:int -> t -> int

  val get_uint32_be : pos:int -> t -> int

  val sub_string : pos:int -> end_:int -> t -> string
end

module type S = sig
  type source

  val read : ?offset:int -> source -> Representation.t
end

module Make (R : Source) : S with type source = R.t
