open Containers
open Representation

module Input = struct
  type event =
    | Tempo_bpm of float
    | Key_sig of { alter : int; mode : key_sig_mode }
    | Time_sig of { num : int; den : int }
    | Note of { channel : int; pitch : int; velocity : int }
  [@@deriving show { with_path = false }, eq, ord]

  type row = {
    onset_beats : float;
    duration_beats : float;
    onset_seconds : float;
    duration_seconds : float;
    track : int;
    event : event;
  }
  [@@deriving show { with_path = false }, eq]

  type t = row list [@@deriving show { with_path = false }, eq]

  let row_compare a b =
    match Float.compare a.onset_beats b.onset_beats with
    | 0 -> (
        match compare_event a.event b.event with
        | 0 -> (
            match Float.compare a.duration_beats b.duration_beats with
            | 0 -> Int.compare a.track b.track
            | x -> x )
        | x -> x )
    | x -> x

  let microseconds_to_seconds x = x *. 1e-6

  type track_cursor = {
    index : int;
    mutable next_onset : int;
    mutable events : track_events;
  }

  let make ?(default_tempo = 500_000) ~ticks tracks =
    let next_onset curtime = function
      | [] -> -1
      | x :: _ -> curtime + delta_time x
    in
    let pop_event cursor =
      match cursor.events with
      | [] -> assert false
      | x :: xs ->
          cursor.events <- xs;
          cursor.next_onset <- next_onset cursor.next_onset xs;
          x
    in
    let add_meta_event beat seconds rows
        (onset_beats, onset_seconds, track, event) =
      rows :=
        {
          onset_beats;
          onset_seconds;
          duration_beats = beat -. onset_beats;
          duration_seconds = seconds -. onset_seconds;
          track;
          event;
        }
        :: !rows
    in
    let add_note ~onset_beats ~onset_seconds ~end_beat ~end_seconds ~track
        ~channel ~pitch ~velocity rows =
      rows :=
        {
          onset_beats;
          onset_seconds;
          duration_beats = end_beat -. onset_beats;
          duration_seconds = end_seconds -. onset_seconds;
          track;
          event = Note { channel; pitch; velocity };
        }
        :: !rows
    in
    let ticks = float_of_int ticks in
    let cursors =
      Array.of_list tracks
      |> Array.mapi (fun index events ->
             { index; next_onset = next_onset 0 events; events })
    in
    let curtime = ref 0 in
    let seconds = ref 0.0 in
    let seconds_per_beat =
      ref (microseconds_to_seconds (float_of_int default_tempo))
    in
    let rows = ref [] in
    let next = ref cursors.(0) in
    let sustained = Hashtbl.create 10 in
    let cur_time_sig = ref None in
    let cur_key_sig = ref None in
    let cur_tempo = ref None in
    let last = Array.length cursors - 1 in
    while !next.next_onset <> -1 do
      for i = 0 to last do
        let cursor = cursors.(i) in
        if cursor.next_onset <> -1 && cursor.next_onset < !next.next_onset then
          next := cursor
      done;
      if !next.next_onset <> -1 then (
        seconds :=
          !seconds
          +. float_of_int (!next.next_onset - !curtime)
             *. !seconds_per_beat /. ticks;
        curtime := !next.next_onset;
        let beat = float_of_int !curtime /. ticks in
        match pop_event !next with
        | Meta_event (_, Tempo t) ->
            Option.iter (add_meta_event beat !seconds rows) !cur_tempo;
            seconds_per_beat := microseconds_to_seconds (float_of_int t);
            cur_tempo :=
              Some
                ( beat,
                  !seconds,
                  !next.index,
                  Tempo_bpm (60. /. !seconds_per_beat) )
        | Meta_event (_, Key_sig { alter; mode }) ->
            Option.iter (add_meta_event beat !seconds rows) !cur_key_sig;
            cur_key_sig :=
              Some (beat, !seconds, !next.index, Key_sig { alter; mode })
        | Meta_event (_, Time_sig { num; den; _ }) ->
            Option.iter (add_meta_event beat !seconds rows) !cur_time_sig;
            cur_time_sig :=
              Some (beat, !seconds, !next.index, Time_sig { num; den })
        | Midi_event
            ( _,
              {
                channel;
                data = Note_off { pitch; _ } | Note_on { pitch; velocity = 0 };
              } ) ->
            Hashtbl.update sustained ~k:(!next.index, channel, pitch)
              ~f:(fun (track, channel, pitch) old ->
                Option.iter
                  (fun (onset_beats, onset_seconds, velocity) ->
                    add_note ~onset_beats ~onset_seconds ~track ~channel ~pitch
                      ~velocity ~end_beat:beat ~end_seconds:!seconds rows)
                  old;
                None)
        | Midi_event (_, { channel; data = Note_on { pitch; velocity } }) ->
            Hashtbl.update sustained ~k:(!next.index, channel, pitch)
              ~f:(fun (track, channel, pitch) old ->
                Option.iter
                  (fun (onset_beats, onset_seconds, velocity) ->
                    add_note ~onset_beats ~onset_seconds ~track ~channel ~pitch
                      ~velocity ~end_beat:beat ~end_seconds:!seconds rows)
                  old;
                Some (beat, !seconds, velocity))
        | _ -> () )
    done;
    let beat = float_of_int !curtime /. ticks in
    Option.iter (add_meta_event beat !seconds rows) !cur_tempo;
    Option.iter (add_meta_event beat !seconds rows) !cur_time_sig;
    Option.iter (add_meta_event beat !seconds rows) !cur_key_sig;
    Hashtbl.iter
      (fun (track, channel, pitch) (onset_beats, onset_seconds, velocity) ->
        add_note ~onset_beats ~onset_seconds ~track ~channel ~pitch ~velocity
          ~end_beat:beat ~end_seconds:!seconds rows)
      sustained;
    List.sort row_compare !rows

  let of_midifile ?default_tempo x =
    match x.division with
    | Ticks ticks ->
        make ?default_tempo ~ticks
          (List.map
             (fun (track : Representation.track) -> track.events)
             x.tracks)
    | SMPTE _ -> invalid_arg "SMPTE not supported"

  let is_monophonic ?(overlap = 0.0) rows =
    let rec loop sustained_until = function
      | [] -> true
      | { onset_beats; duration_beats; event = Note _; _ } :: rest ->
          if Float.(onset_beats +. overlap < sustained_until) then false
          else loop (onset_beats +. duration_beats) rest
      | _ :: rest -> loop sustained_until rest
    in
    loop 0.0 rows
end
