module type Sink = sig
  type t

  val set_uint8 : pos:int -> t -> int -> unit

  val set_uint16_be : pos:int -> t -> int -> unit

  val set_uint32_be : pos:int -> t -> int -> unit

  val blit_string : pos:int -> t -> string -> unit
end

module type S = sig
  type sink

  val write : ?offset:int -> sink -> Representation.t -> int
end

module Make (W : Sink) : S with type sink = W.t
