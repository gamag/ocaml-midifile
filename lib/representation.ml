open Containers

type file_format = Single_track | Simultaneous_tracks | Sequential_tracks
[@@deriving show { with_path = false }, eq]

type midi_message_data =
  | Note_off of { pitch : int; velocity : int }
  | Note_on of { pitch : int; velocity : int }
  | Polyphonic_pressure of { pitch : int; pressure : int }
  | Control_change of { controller : int; value : int }
  | Program_change of int
  | Channel_pressure of int
  | Pitch_wheel_change of int
[@@deriving show { with_path = false }, eq]

type midi_message = { channel : int; data : midi_message_data }
[@@deriving show { with_path = false }, eq]

type smpte_fps = Fps_24 | Fps_25 | Fps_30_drop_frame | Fps_30_non_drop_frame
[@@deriving show { with_path = false }, eq]

type key_sig_mode = Major_key | Minor_key
[@@deriving show { with_path = false }, eq, ord]

type meta_message =
  | SMPTE_offset of {
      fps : smpte_fps;
      hour : int;
      minute : int;
      second : int;
      frame : int;
      subframe : int;
    }
  (* time 0;
     format1 - Only in first track *)
  | End_of_track
  | Sequence_number of int option
  (* time 0;
     format1 - Only in first track *)
  | Midi_channel_prefix of int
  | Text of string
  | Copyright of string
  (* time 0;
     Only in first track *)
  | Sequence_track_name of string
  | Instrument_name of string
  | Midi_port of int
  | Tempo of int
  (* format1 - Only in first track *)
  | Lyric of string
  | Marker of string
  (* format1 - Only in first track *)
  | Cue_point of string
  | Program_name of string
  | Device_name of string
  (* time 0 *)
  | Key_sig of { alter : int; mode : key_sig_mode } (* format1 - Only in first track *)
  | Time_sig of { num : int; den : int; clocks : int; div : int }
  (* format1 - Only in first track *)
  | Sequencer_specific_data of string
[@@deriving show { with_path = false }, eq]

type sysex_message =
  | Escaped of string
  | Single_packet of string
  | Multi_packet_begin of string
  | Multi_packet_middle of string
  | Multi_packet_end of string
[@@deriving show { with_path = false }, eq]

type track_event =
  | Midi_event of int * midi_message
  | Meta_event of int * meta_message
  | Sysex_event of int * sysex_message
[@@deriving show { with_path = false }, eq]

type division = Ticks of int | SMPTE of smpte_fps * int
[@@deriving show { with_path = false }, eq]

type track_events = track_event list [@@deriving show { with_path = false }, eq]

type track = { size_in_bytes : int; events : track_events }
[@@deriving show { with_path = false }, eq]

type t = { format : file_format; division : division; tracks : track list }
[@@deriving show { with_path = false }, eq]

let ensure_range tag min max x =
  if x < min || x > max then
    invalid_arg
      (Printf.sprintf "value (%d) outside of range (%d, %d) for %s" x min max
         tag)

let max_uint32 = Option.get_exn Int32.(unsigned_to_int minus_one)

let ensure_uint7 tag = ensure_range tag 0 127

let ensure_uint32 tag = ensure_range tag 0 max_uint32

let note_off ~pitch ~velocity =
  ensure_uint7 "Note_off.pitch" pitch;
  ensure_uint7 "Note_off.velocity" velocity;
  Note_off { pitch; velocity }

let note_on ~pitch ~velocity =
  ensure_uint7 "Note_on.pitch" pitch;
  ensure_uint7 "Note_on.velocity" velocity;
  Note_on { pitch; velocity }

let polyphonic_pressure ~pitch ~pressure =
  ensure_uint7 "Polyphonic_pressure.pitch" pitch;
  ensure_uint7 "Polyphonic_pressure.pressure" pressure;
  Polyphonic_pressure { pitch; pressure }

let control_change ~controller ~value =
  ensure_uint7 "Control_change.controller" controller;
  ensure_uint7 "Control_change.value" value;
  Control_change { controller; value }

let program_change x =
  ensure_uint7 "Program_change" x;
  Program_change x

let channel_pressure x =
  ensure_uint7 "Channel_pressure" x;
  Channel_pressure x

let pitch_wheel_change x =
  ensure_uint7 "Pitch_wheel_change" x;
  Pitch_wheel_change x

let ticks x =
  ensure_range "Ticks" 1 0x7fff x;
  Ticks x

let smpte fps subframes =
  if subframes > 0xff || subframes < 0 then
    invalid_arg
      ("invalid SMPTE divison subframes value: " ^ string_of_int subframes);
  SMPTE (fps, subframes)

let validate_uint16 x =
  if x > 0xffff || x < 0 then
    invalid_arg ("invalid uint16 value: " ^ string_of_int x)

let sequence_number x =
  Option.iter validate_uint16 x;
  Sequence_number x

let text x = Text x

let copyright x = Copyright x

let sequence_track_name x = Sequence_track_name x

let instrument_name x = Instrument_name x

let lyric x = Lyric x

let marker x = Marker x

let cue_point x = Cue_point x

let program_name x = Program_name x

let device_name x = Device_name x

let midi_channel_prefix channel =
  ensure_range "Midi_channel_prefix" 0 15 channel;
  Midi_channel_prefix channel

let midi_port x =
  ensure_uint7 "Midi_port" x;
  Midi_port x

let end_of_track = End_of_track

let tempo x =
  ensure_range "tempo" 0 0xffffff x;
  Tempo x

let smpte_offset ~hour ~minute ~second ~frame ~subframe fps =
  ensure_range "SMPTE_offset.hour" 0 23 hour;
  ensure_range "SMPTE_offset.minute" 0 59 minute;
  ensure_range "SMPTE_offset.second" 0 59 second;
  let max_frame =
    match fps with
    | Fps_24 -> 23
    | Fps_25 -> 24
    | Fps_30_drop_frame -> 28
    | Fps_30_non_drop_frame -> 29
  in
  ensure_range "SMPTE_offset.frame" 0 max_frame frame;
  ensure_range "SMPTE_offset.subframe" 0 0xff frame;
  SMPTE_offset { fps; hour; minute; second; frame; subframe }

let is_power_of_2 x = x <> 0 && x land (lnot x + 1) = x

let time_sig ~num ~den ~clocks ~div =
  ensure_range "Time_sig.num" 0 0xff num;
  ensure_range "Time_sig.clocks" 0 0xff clocks;
  if not (den > 0 && is_power_of_2 den) then
    invalid_arg ("invalid Time_sig.den value: " ^ string_of_int den);
  ensure_range "Time_sig.div" 0 0xff div;
  Time_sig { num; den; clocks; div }

let key_sig alter mode =
  ensure_range "Key_sig.alter" (-7) 7 alter;
  Key_sig { alter; mode }

let sequencer_specific_data x = Sequencer_specific_data x

let ensure_delta_time x = ensure_range "delta_time" 0 0xffffffff x

let midi_message channel data =
  ensure_range "midi_event.channel" 0 15 channel;
  { channel; data }

let escaped data = Escaped data

let single_packet data = Single_packet data

let multi_packet_begin data = Multi_packet_begin data

let multi_packet_middle data = Multi_packet_middle data

let multi_packet_end data = Multi_packet_end data

let midi_event delta_time message =
  ensure_delta_time delta_time;
  Midi_event (delta_time, message)

let sysex_event delta_time message =
  ensure_delta_time delta_time;
  Sysex_event (delta_time, message)

let meta_event delta_time data =
  ensure_delta_time delta_time;
  Meta_event (delta_time, data)

let track_events_size_in_bytes track =
  let var_int32_size x =
    let x = ref (x lsr 7) in
    let count = ref 1 in
    while !x > 0 do
      incr count;
      x := !x lsr 7
    done;
    !count
  in
  let string_size x =
    let length = String.length x in
    var_int32_size length + length
  in
  let meta_message_size = function
    | End_of_track | Sequence_number None -> 2
    | Midi_port _ | Midi_channel_prefix _ -> 3
    | Sequence_number (Some _) | Key_sig _ -> 4
    | Tempo _ -> 5
    | Time_sig _ -> 6
    | SMPTE_offset _ -> 7
    | Cue_point x
    | Program_name x
    | Device_name x
    | Marker x
    | Lyric x
    | Text x
    | Copyright x
    | Sequence_track_name x
    | Instrument_name x
    | Sequencer_specific_data x ->
        1 + string_size x
  in
  let sysex_message_size = function
    | Multi_packet_begin x
    | Multi_packet_middle x
    | Multi_packet_end x
    | Single_packet x
    | Escaped x ->
        1 + string_size x
  in
  let data_size_and_status ch = function
    | Note_off _ -> (2, 0x80 lor ch)
    | Note_on _ -> (2, 0x90 lor ch)
    | Polyphonic_pressure _ -> (2, 0xa0 lor ch)
    | Control_change _ -> (2, 0xb0 lor ch)
    | Program_change _ -> (1, 0xc0 lor ch)
    | Channel_pressure _ -> (1, 0xd0 lor ch)
    | Pitch_wheel_change _ -> (1, 0xe0 lor ch)
  in
  let event_size_in_bytes running_status = function
    | Midi_event (delta_time, { channel; data }) ->
        let data_size, status = data_size_and_status channel data in
        ( ( var_int32_size delta_time
          + if status = running_status then data_size else data_size + 1 ),
          status )
    | Sysex_event (delta_time, message) ->
        (var_int32_size delta_time + 1 + sysex_message_size message, 0)
    | Meta_event (delta_time, message) ->
        (var_int32_size delta_time + 1 + meta_message_size message, 0)
  in
  List.fold_left
    (fun (acc, running_status) event ->
      let ev_size, running_status = event_size_in_bytes running_status event in
      (acc + ev_size, running_status))
    (0, 0) track
  |> fst

let track events =
  let size_in_bytes = track_events_size_in_bytes events in
  ensure_uint32 "track.size_in_bytes" size_in_bytes;
  { size_in_bytes; events }

let make format division tracks = { format; division; tracks }

let delta_time = function
  | Midi_event (x, _) | Meta_event (x, _) | Sysex_event (x, _) -> x

let size_in_bytes { format = _; division = _; tracks } =
  let header_size = 14 in
  let chunk_header_size = 8 in
  List.fold_left
    (fun acc track -> chunk_header_size + track.size_in_bytes + acc)
    header_size tracks
