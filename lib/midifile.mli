include module type of Representation

val read_from_bytes : ?offset:int -> bytes -> t

val read_from_channel : in_channel -> t

val read_from_file : string -> t

val write_to_bytes : ?offset:int -> bytes -> t -> int

val write_to_new_bytes : t -> bytes

val write_to_channel : out_channel -> t -> int

val write_to_file : ?mode:int -> string -> t -> int

module Reader : module type of Reader

module Writer : module type of Writer

module Representation : module type of Representation

module Simple_event_list : module type of Simple_event_list
