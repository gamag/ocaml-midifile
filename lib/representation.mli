type file_format = Single_track | Simultaneous_tracks | Sequential_tracks
[@@deriving show { with_path = false }, eq]

type midi_message_data = private
  | Note_off of { pitch : int; velocity : int }
  | Note_on of { pitch : int; velocity : int }
  | Polyphonic_pressure of { pitch : int; pressure : int }
  | Control_change of { controller : int; value : int }
  | Program_change of int
  | Channel_pressure of int
  | Pitch_wheel_change of int
[@@deriving show { with_path = false }, eq]

val note_off : pitch:int -> velocity:int -> midi_message_data

val note_on : pitch:int -> velocity:int -> midi_message_data

val polyphonic_pressure : pitch:int -> pressure:int -> midi_message_data

val control_change : controller:int -> value:int -> midi_message_data

val program_change : int -> midi_message_data

val channel_pressure : int -> midi_message_data

val pitch_wheel_change : int -> midi_message_data

type midi_message = private { channel : int; data : midi_message_data }
[@@deriving show { with_path = false }, eq]

val midi_message : int -> midi_message_data -> midi_message

type smpte_fps = Fps_24 | Fps_25 | Fps_30_drop_frame | Fps_30_non_drop_frame
[@@deriving show { with_path = false }, eq]

type key_sig_mode = Major_key | Minor_key
[@@deriving show { with_path = false }, eq, ord]

type meta_message = private
  | SMPTE_offset of {
      fps : smpte_fps;
      hour : int;
      minute : int;
      second : int;
      frame : int;
      subframe : int;
    }
  | End_of_track
  | Sequence_number of int option
  | Midi_channel_prefix of int
  | Text of string
  | Copyright of string
  | Sequence_track_name of string
  | Instrument_name of string
  | Midi_port of int
  | Tempo of int
  | Lyric of string
  | Marker of string
  | Cue_point of string
  | Program_name of string
  | Device_name of string
  | Key_sig of { alter : int; mode : key_sig_mode }
  | Time_sig of { num : int; den : int; clocks : int; div : int }
  | Sequencer_specific_data of string
[@@deriving show { with_path = false }, eq]

val smpte_offset :
  hour:int ->
  minute:int ->
  second:int ->
  frame:int ->
  subframe:int ->
  smpte_fps ->
  meta_message

val end_of_track : meta_message

val sequence_number : int option -> meta_message

val midi_channel_prefix : int -> meta_message

val text : string -> meta_message

val copyright : string -> meta_message

val sequence_track_name : string -> meta_message

val instrument_name : string -> meta_message

val midi_port : int -> meta_message

val tempo : int -> meta_message

val lyric : string -> meta_message

val marker : string -> meta_message

val cue_point : string -> meta_message

val program_name : string -> meta_message

val device_name : string -> meta_message

val key_sig : int -> key_sig_mode -> meta_message

val time_sig : num:int -> den:int -> clocks:int -> div:int -> meta_message

val sequencer_specific_data : string -> meta_message

type sysex_message = private
  | Escaped of string
  | Single_packet of string
  | Multi_packet_begin of string
  | Multi_packet_middle of string
  | Multi_packet_end of string
[@@deriving show { with_path = false }, eq]

val escaped : string -> sysex_message

val single_packet : string -> sysex_message

val multi_packet_begin : string -> sysex_message

val multi_packet_middle : string -> sysex_message

val multi_packet_end : string -> sysex_message

type track_event = private
  | Midi_event of int * midi_message
  | Meta_event of int * meta_message
  | Sysex_event of int * sysex_message
[@@deriving show { with_path = false }, eq]

val midi_event : int -> midi_message -> track_event

val sysex_event : int -> sysex_message -> track_event

val meta_event : int -> meta_message -> track_event

val delta_time : track_event -> int

type division = private Ticks of int | SMPTE of smpte_fps * int
[@@deriving show { with_path = false }, eq]

val ticks : int -> division

val smpte : smpte_fps -> int -> division

type track_events = track_event list [@@deriving show { with_path = false }, eq]

type track = private { size_in_bytes : int; events : track_events }
[@@deriving show { with_path = false }, eq]

val track : track_events -> track

type t = private {
  format : file_format;
  division : division;
  tracks : track list;
}
[@@deriving show { with_path = false }, eq]

val make : file_format -> division -> track list -> t

val size_in_bytes : t -> int
