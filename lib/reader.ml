open Containers
open Representation

exception Invalid_header_prefix of string * int

exception Invalid_header of int * int * int

exception Invalid_status of int

exception Invalid_meta_event of int * int

module type Source = sig
  type t

  val get_uint8 : pos:int -> t -> int

  val get_uint16_be : pos:int -> t -> int

  val get_uint32_be : pos:int -> t -> int

  val sub_string : pos:int -> end_:int -> t -> string
end

module type S = sig
  type source

  val read : ?offset:int -> source -> t
end

let division_of_uint16 x =
  match x land 0x8000 with
  | 0 -> ticks x
  | _ ->
      smpte
        ( match x land 0xff00 with
        | 0xe600 -> Fps_24
        | 0xe700 -> Fps_25
        | 0xe800 -> Fps_30_drop_frame
        | 0xe900 -> Fps_30_non_drop_frame
        | _ -> invalid_arg ("invalid division: " ^ string_of_int x) )
        (x land 0x00ff)

module Make (R : Source) : S with type source = R.t = struct
  type source = R.t

  let get_int24_be ~pos b =
    (R.get_uint8 b ~pos lsl 16)
    lor (R.get_uint8 b ~pos:(pos + 1) lsl 8)
    lor R.get_uint8 b ~pos:(pos + 2)

  let read_var_int32 ~pos b =
    let i = ref pos in
    let ch = ref (R.get_uint8 b ~pos:!i) in
    let x = ref (0x7f land !ch) in
    while !ch >= 0x80 do
      incr i;
      ch := R.get_uint8 b ~pos:!i;
      x := (!x lsl 7) lor (0x7f land !ch)
    done;
    (!x, !i + 1)

  let read_string ~pos b =
    let len, pos = read_var_int32 ~pos b in
    let end_ = pos + len in
    (R.sub_string b ~pos ~end_, end_)

  let read_meta_event ~pos b =
    match (R.get_uint8 b ~pos, R.get_uint8 b ~pos:(pos + 1)) with
    | 0x00, 0x00 -> (sequence_number None, pos + 2)
    | 0x00, 0x02 ->
        let number = R.get_uint16_be b ~pos:(pos + 2) in
        (sequence_number (Some number), pos + 4)
    | 0x01, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (text txt, pos)
    | 0x02, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (copyright txt, pos)
    | 0x03, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (sequence_track_name txt, pos)
    | 0x04, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (instrument_name txt, pos)
    | 0x05, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (lyric txt, pos)
    | 0x06, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (marker txt, pos)
    | 0x07, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (cue_point txt, pos)
    | 0x08, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (program_name txt, pos)
    | 0x09, _ ->
        let txt, pos = read_string b ~pos:(pos + 1) in
        (device_name txt, pos)
    | 0x20, 0x01 ->
        let ch = R.get_uint8 b ~pos:(pos + 2) in
        (midi_channel_prefix ch, pos + 3)
    | 0x21, 0x01 ->
        let port = R.get_uint8 b ~pos:(pos + 2) in
        (midi_port port, pos + 3)
    | 0x2f, 0x00 -> (end_of_track, pos + 2)
    | 0x51, 0x03 ->
        let value = get_int24_be b ~pos:(pos + 2) in
        (tempo value, pos + 5)
    | 0x54, 0x05 ->
        let fps_hour = R.get_uint8 b ~pos:(pos + 2) in
        let minute = R.get_uint8 b ~pos:(pos + 3) in
        let second = R.get_uint8 b ~pos:(pos + 4) in
        let frame = R.get_uint8 b ~pos:(pos + 5) in
        let subframe = R.get_uint8 b ~pos:(pos + 6) in
        let fps =
          match fps_hour land 0b01100000 with
          | 0b0_00_00000 -> Fps_24
          | 0b0_01_00000 -> Fps_25
          | 0b0_10_00000 -> Fps_30_drop_frame
          | 0x0_11_00000 -> Fps_30_non_drop_frame
          | x ->
              invalid_arg ("invalid int in smpte_fps_of_int: " ^ string_of_int x)
        in
        let hour = fps_hour land 0x00011111 in
        (smpte_offset fps ~hour ~minute ~second ~frame ~subframe, pos + 7)
    | 0x58, 0x04 ->
        let num = R.get_uint8 b ~pos:(pos + 2) in
        let den = Int.pow 2 (R.get_uint8 b ~pos:(pos + 3)) in
        let clocks = R.get_uint8 b ~pos:(pos + 4) in
        let div = R.get_uint8 b ~pos:(pos + 5) in
        (time_sig ~num ~den ~clocks ~div, pos + 6)
    | 0x59, 0x02 ->
        let alter = R.get_uint8 b ~pos:(pos + 2) in
        let mode =
          match R.get_uint8 b ~pos:(pos + 3) with
          | 0 -> Major_key
          | 1 -> Minor_key
          | _ -> invalid_arg "key sig"
        in
        (key_sig alter mode, pos + 4)
    | 0x7f, _ ->
        let txt, pos = read_string ~pos:(pos + 1) b in
        (sequencer_specific_data txt, pos)
    | a, b -> raise (Invalid_meta_event (a, b))

  let read_midi_event ~pos b status =
    let ch = status land 0x0f in
    match status land 0xf0 with
    | 0x80 ->
        let pitch = R.get_uint8 b ~pos in
        let velocity = R.get_uint8 b ~pos:(pos + 1) in
        (midi_message ch (note_off ~pitch ~velocity), pos + 2)
    | 0x90 ->
        let pitch = R.get_uint8 b ~pos in
        let velocity = R.get_uint8 b ~pos:(pos + 1) in
        (midi_message ch (note_on ~pitch ~velocity), pos + 2)
    | 0xa0 ->
        let pitch = R.get_uint8 b ~pos in
        let pressure = R.get_uint8 b ~pos:(pos + 2) in
        (midi_message ch (polyphonic_pressure ~pitch ~pressure), pos + 2)
    | 0xb0 ->
        let controller = R.get_uint8 b ~pos in
        let value = R.get_uint8 b ~pos:(pos + 1) in
        (midi_message ch (control_change ~controller ~value), pos + 2)
    | 0xc0 ->
        let program = R.get_uint8 b ~pos in
        (midi_message ch (program_change program), pos + 1)
    | 0xd0 ->
        let pressure = R.get_uint8 b ~pos in
        (midi_message ch (channel_pressure pressure), pos + 1)
    | 0xe0 ->
        let pressure = R.get_uint8 b ~pos in
        (midi_message ch (pitch_wheel_change pressure), pos + 1)
    | _ -> raise (Invalid_status status)

  let rec read_events acc b prev ~pos ~end_ =
    let rec read_until_sysex_multi_packet_end ~pos b acc =
      let delta_time, pos = read_var_int32 b ~pos in
      match R.get_uint8 b ~pos with
      | 0xf7 -> (
          let txt, pos = read_string b ~pos:(pos + 1) in
          match txt.[String.length txt - 1] with
          | '\xf7' -> (sysex_event delta_time (multi_packet_end txt) :: acc, pos)
          | _ ->
              read_until_sysex_multi_packet_end ~pos b
                (sysex_event delta_time (multi_packet_end txt) :: acc) )
      | x -> raise (Invalid_status x)
    in
    if pos >= end_ then List.rev acc
    else
      let delta_time, pos = read_var_int32 b ~pos in
      let status = R.get_uint8 b ~pos in
      let status, pos =
        if status land 0x80 <> 0 then (status, pos) else (prev, pos - 1)
      in
      let acc, pos =
        match status with
        | 0xf0 -> (
            let txt, pos = read_string b ~pos:(pos + 1) in
            match txt.[String.length txt - 1] with
            | '\xf7' -> (sysex_event delta_time (single_packet txt) :: acc, pos)
            | _ ->
                read_until_sysex_multi_packet_end ~pos b
                  (sysex_event delta_time (multi_packet_begin txt) :: acc) )
        | 0xf7 ->
            let txt, pos = read_string ~pos:(pos + 1) b in
            (sysex_event delta_time (escaped txt) :: acc, pos)
        | 0xff ->
            let event, pos = read_meta_event ~pos:(pos + 1) b in
            (meta_event delta_time event :: acc, pos)
        | status ->
            let event, pos = read_midi_event ~pos:(pos + 1) b status in
            (midi_event delta_time event :: acc, pos)
      in
      read_events acc b status ~pos ~end_

  let rec read_tracks ~track_index ~ntracks ~pos b =
    if track_index = ntracks then []
    else
      let chunk_type = R.sub_string b ~pos ~end_:4 in
      let length = R.get_uint32_be b ~pos:(pos + 4) in
      let pos = pos + 8 in
      let end_ = pos + length in
      match chunk_type with
      | "MTrk" ->
          track (read_events [] b 0 ~pos ~end_)
          :: read_tracks ~track_index:(track_index + 1) ~ntracks ~pos:end_ b
      | _ -> read_tracks ~track_index ~ntracks ~pos:end_ b

  let read_header offset b =
    let chunk_type_end = offset + 4 in
    let chunk_header_end = chunk_type_end + 4 in
    let header_end =
      match
        R.
          ( sub_string b ~pos:offset ~end_:chunk_type_end,
            get_uint32_be b ~pos:chunk_type_end )
      with
      | "MThd", size when size >= 6 -> chunk_header_end + size
      | chunk_type, size -> raise (Invalid_header_prefix (chunk_type, size))
    in
    match
      R.
        ( get_uint16_be b ~pos:chunk_header_end,
          get_uint16_be b ~pos:(chunk_header_end + 2),
          get_uint16_be b ~pos:(chunk_header_end + 4) )
    with
    | 0, 1, div -> (Single_track, 1, division_of_uint16 div, header_end)
    | 1, ntracks, div ->
        (Simultaneous_tracks, ntracks, division_of_uint16 div, header_end)
    | 2, ntracks, div ->
        (Sequential_tracks, ntracks, division_of_uint16 div, header_end)
    | c, d, e -> raise (Invalid_header (c, d, e))

  let read ?(offset = 0) b =
    let format, ntracks, division, pos = read_header offset b in
    make format division (read_tracks ~track_index:0 ~ntracks ~pos b)
end
