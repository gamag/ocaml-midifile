open Containers
module Representation = Representation
include Representation

module Bytes_io = struct
  module Source = struct
    type t = Bytes.t

    let get_uint8 ~pos b = Bytes.get_uint8 b pos

    let get_uint16_be ~pos b = Bytes.get_uint16_be b pos

    let get_uint32_be ~pos b =
      Bytes.get_int32_be b pos |> Int32.unsigned_to_int |> Option.get_exn

    let sub_string ~pos ~end_ b = Bytes.sub_string b pos end_
  end

  module Sink = struct
    type t = Bytes.t

    let set_uint8 ~pos b x = Bytes.set_uint8 b pos x

    let set_uint16_be ~pos b x = Bytes.set_uint16_be b pos x

    let set_uint32_be ~pos b x = Bytes.set_int32_be b pos (Int32.of_int x)

    let blit_string ~pos b x = Bytes.blit_string x 0 b pos (String.length x)
  end

  include Reader.Make (Source)
  include Writer.Make (Sink)
end

let read_from_bytes = Bytes_io.read

let read_from_channel ch = CCIO.read_all_bytes ch |> read_from_bytes

let read_from_file file = CCIO.with_in file read_from_channel

let write_to_bytes = Bytes_io.write

let write_to_new_bytes midi =
  let bytes = Bytes.create (size_in_bytes midi) in
  ignore (write_to_bytes bytes midi);
  bytes

let write_to_channel ch midi =
  let bytes = write_to_new_bytes midi in
  output_bytes ch bytes;
  Bytes.length bytes

let write_to_file ?mode file midi =
  CCIO.with_out ?mode file (fun ch -> write_to_channel ch midi)

module Reader = Reader
module Writer = Writer
module Simple_event_list = Simple_event_list
